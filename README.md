# Record Manager Refactor

A university had an application made to store student allowance records. Later on they decided to give poor students double the allowance for learning materials. The head teacher was fired and his replacement decided to use the same application for storing teacher allowances. One of your junior developers started working on the task but quit due to frustration. As the university lead developer it is your job to sort out this mess.

# Prerequisites
- PHP 5.6+ (PHP 7 recommended)
- SQLite3 PHP extension
- Composer

# Installation
1. Clone the project: `git clone ssh://git@bitbucket.gameforge.com:7999/tm/exercise-refactor_record_service.git`
2. Change to the project directory: `cd exercise-refactor_record_service`
3. Enter `composer install` and press enter. 

# Testing
- To test use the command: `./vendor/bin/phpunit`