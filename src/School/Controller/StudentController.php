<?php
namespace School\Controller;

class StudentController
{
    private $manager;
 
    public function __construct(\School\Manager\RecordManager $manager)
    {
        $this->manager = $manager;
    }
 
    public function addStudentRecordAction($studentId, $allowance = 100)
    {
        $this->manager->add($studentId, $allowance);
    }
 
    public function addTeacherRecordAction($teacherId, $allowance = 1000)
    {
        // add call to manager here
    }
}
