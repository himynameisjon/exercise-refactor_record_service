<?php
namespace School\Model;

class StudentRecord
{
    /** @var string UUID */
    private $identifier;

    /** @var int Student credit allowance */
    private $allowance;
 
    public function setId($identifier)
    {
        $this->identifier = $identifier;
    }
 
    public function getId()
    {
        return $this->identifier;
    }
 
    public function setAllowance($allowance)
    {
        $this->allowance = $allowance;
    }
 
    public function getAllowance()
    {
        return $this->allowance;
    }
}
