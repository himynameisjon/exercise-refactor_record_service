<?php
namespace School\DB;

class SQLite
{
    private static $connection;

    public static function getConnection()
    {
        if (!self::$connection) {
            self::$connection = self::createConnection();
            self::setupDatabases(self::$connection);
        }

        return self::$connection;
    }

    private static function createConnection()
    {
        $config = new \Doctrine\DBAL\Configuration();
        $connectionParams = [
            'driver' => 'pdo_sqlite',
            'memory' => true,
        ];

        return \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $config);
    }

    private static function setupDatabases($connection)
    {
        $schema = new \Doctrine\DBAL\Schema\Schema();
        $platform = $connection->getDatabasePlatform();

        $records = $schema->createTable('records');
        $records->addColumn('id', 'string', ['length' => 60]);
        $records->addColumn('allowance', 'integer', ['signed' => true]);
        $records->setPrimaryKey(['id']);

        $records = $schema->createTable('students');
        $records->addColumn('id', 'string', ['length' => 60]);
        $records->addColumn('isPoor', 'boolean');
        $records->setPrimaryKey(['id']);

        $records = $schema->createTable('teachers');
        $records->addColumn('id', 'string', ['length' => 60]);
        $records->addColumn('hasTenure', 'boolean');
        $records->setPrimaryKey(['id']);

        foreach ($schema->toSql($platform) as $query) {
            $connection->executeQuery($query);
        }
    }
}
