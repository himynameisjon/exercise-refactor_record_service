<?php
namespace School\Repository;

class StudentRepository
{
    private $connection;

    public function __construct(\Doctrine\DBAL\Connection $connection)
    {
        $this->connection = $connection;
    }

    public function exists($studentId)
    {
        return $this->getStudent($studentId) !== false;
    }

    public function find($studentId)
    {
        return $this->getStudent($studentId);
    }

    private function getStudent($studentId)
    {
        return $this->connection->fetchAssoc('SELECT * FROM students WHERE id = ? LIMIT 1', [$studentId]);
    }
}
