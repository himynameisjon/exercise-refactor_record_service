<?php
namespace School\Repository;

class RecordRepository
{
    private $connection;

    public function __construct(\Doctrine\DBAL\Connection $connection)
    {
        $this->connection = $connection;
    }

    public function add($record)
    {
        $data = [
            'id' => $record->getId(),
            'allowance' => $record->getAllowance(),
        ];

        $this->connection->insert('records', $data);
    }

    public function exists($recordId)
    {
        return $this->connection->fetchAssoc('SELECT * FROM records WHERE id = ? LIMIT 1', [$recordId]) !== false;
    }
}
