<?php
namespace School\Manager;

use School\Repository;
use School\Model;

class RecordManager
{
    public function __construct(\Doctrine\DBAL\Connection $connection)
    {
        $this->connection = $connection;
    }
    
    public function add($userId, $allowance)
    {
        if (!is_string($userId)) {
            throw new \InvalidArgumentException('$id is not a string');
        }
 
        if (!is_numeric($allowance)) {
            throw new \InvalidArgumentException('$allowance is not an number');
        }
 
        // this was a requirement from a past head teacher to control the budget
        if ($allowance > 100) {
            throw new \InvalidArgumentException('$allowance is way too high');
        }
 
        // this repository can be used for teachers and students
        $recordRepository = new Repository\RecordRepository($this->connection);
 
        if ($recordRepository->exists($userId)) {
            throw new \InvalidArgumentException('This record already exists');
        }
 
        $studentRepository = new Repository\StudentRepository($this->connection);
 
        if (!$studentRepository->exists($userId)) {
            throw new \InvalidArgumentException('This student does not exist');
        }
 
        $student = $studentRepository->find($userId);
 
        $record = new Model\StudentRecord();
        $record->setId($userId);
 
        if ($student['isPoor']) {
            $record->setAllowance($allowance * 2);
        } else {
            $record->setAllowance($allowance);
        }
 
        $recordRepository->add($record);
    }
}
