<?php

class StudentControllerTest extends \PHPUnit_Framework_TestCase
{
    private $controller;
    private $connection;

    const DEFAULT_ID = '1298163e-8914-4ec3-83fa-8c13f5bb7793';

    public function testControllerExists()
    {
        $this->assertInstanceOf(\School\Controller\StudentController::class, $this->controller);
    }

    public function testStudentNormalAllowance()
    {
        $allowance = 100;
        
        $this->addDatabaseEntry('students', [
            'id' => self::DEFAULT_ID,
            'isPoor' => false,
        ]);

        $this->controller->addStudentRecordAction(self::DEFAULT_ID, $allowance);

        $record = $this->getRecord(self::DEFAULT_ID);

        $this->assertEquals($record['allowance'], $allowance);
    }

    public function testPoorStudentDoubleAllowance()
    {
        $allowance = 100;
        
        $this->addDatabaseEntry('students', [
            'id' => self::DEFAULT_ID,
            'isPoor' => true,
        ]);

        $this->controller->addStudentRecordAction(self::DEFAULT_ID, $allowance);

        $record = $this->getRecord(self::DEFAULT_ID);

        $this->assertEquals($record['allowance'], $allowance * 2);
    }

    public function testStudentHighAllowance()
    {
        $allowance = 101;
        
        $this->addDatabaseEntry('students', [
            'id' => self::DEFAULT_ID,
            'isPoor' => true,
        ]);

        $this->expectException(\InvalidArgumentException::class);
        $this->assertTrue($this->getRecord(self::DEFAULT_ID) === false);
        $this->controller->addStudentRecordAction(self::DEFAULT_ID, $allowance);
    }

    public function testTeacherNormalAllowance()
    {
        $allowance = 1000;
        
        $this->addDatabaseEntry('teachers', [
            'id' => self::DEFAULT_ID,
            'hasTenure' => false,
        ]);

        $this->controller->addTeacherRecordAction(self::DEFAULT_ID, $allowance);

        $record = $this->getRecord(self::DEFAULT_ID);

        $this->assertEquals($record['allowance'], $allowance);
    }

    public function testTeacherTenuredAllowance()
    {
        $allowance = 1000;
        
        $this->addDatabaseEntry('teachers', [
            'id' => self::DEFAULT_ID,
            'hasTenure' => true,
        ]);

        $this->controller->addTeacherRecordAction(self::DEFAULT_ID, $allowance);

        $record = $this->getRecord(self::DEFAULT_ID);

        $this->assertEquals($record['allowance'], $allowance * 10);
    }

    public function testTeacherHighAllowance()
    {
        $allowance = 1001;
        
        $this->addDatabaseEntry('students', [
            'id' => self::DEFAULT_ID,
            'isPoor' => true,
        ]);

        $this->expectException(\InvalidArgumentException::class);
        $this->controller->addTeacherRecordAction(self::DEFAULT_ID, $allowance);
        $this->assertTrue($this->getRecord(self::DEFAULT_ID) === false);
    }

    /**
     * This method mimics the functionality of an IoC container. Ignore this section
     * and the smelly DB code while refactoring.
     *
     * @SuppressWarnings(PHPMD)
     */
     
    protected function setUp()
    {
        $this->connection = \School\DB\SQLite::getConnection();
        $this->controller = new \School\Controller\StudentController(new \School\Manager\RecordManager($this->connection));

        $this->connection->executeQuery('DELETE FROM records');
        $this->connection->executeQuery('DELETE FROM students');
        $this->connection->executeQuery('DELETE FROM teachers');
    }

    private function addDatabaseEntry($table, $data)
    {
        $this->connection->insert($table, $data);
    }

    private function getRecord($recordId)
    {
        return $this->connection->fetchAssoc('SELECT * FROM records WHERE id = ?', [$recordId]);
    }
}
